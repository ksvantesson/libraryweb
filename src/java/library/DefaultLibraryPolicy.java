package library;

import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class DefaultLibraryPolicy implements LibraryPolicy {

    @Override
    public int getMaxNumBorrowedBooks() {
        return 5;
    }

    @Override
    public Calendar calculateDueDate() {
        
        Calendar dueDate = new GregorianCalendar();
        
        dueDate.add(Calendar.DAY_OF_MONTH, 14);
        
        return dueDate;
    }
    
}
