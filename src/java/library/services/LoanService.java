package library.services;

import library.LibraryException;
import library.LibraryPolicy;
import library.models.Borrower;
import library.models.Loan;
import library.repositories.LoanRepository;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class LoanService {
    
    @Inject private BookService bookService;
    @Inject private BorrowerService borrowerService;
    @Inject private LoanRepository loanRepository;
    @Inject private LibraryPolicy policy;

    public Loan createLoan(int bookId, int borrowerId) throws LibraryException {
        
        Borrower borrower = borrowerService.getBorrower(borrowerId);
        
        verifyMayBorrow(borrower);
        
        Loan loan = null; //new Loan(bookService.getBook(bookId), borrower, policy.calculateDueDate());
        
        loanRepository.add(loan);
        
        return loan;
    }

    private void verifyMayBorrow(Borrower borrower) throws LibraryException {
        
        List<Loan> loans = loanRepository.find(borrower);
        
        verifyNumberOfBorrowedBooks(loans);
        verifyDueDates(loans);
    }

    private void verifyNumberOfBorrowedBooks(List<Loan> loans) throws LibraryException {

        if (loans.size() >= policy.getMaxNumBorrowedBooks())
            throw new LibraryException(String.format("Låntagaren har redan lånat %d böcker.", policy.getMaxNumBorrowedBooks()));
    }

    private void verifyDueDates(List<Loan> loans) throws LibraryException {

        GregorianCalendar now = new GregorianCalendar();
        
        for (Loan loan : loans) {
            
            if (now.after(loan.getDueDate())) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                throw new LibraryException(String.format("Låntagaren har ett lån som skulle varit återlämnat %s.", dateFormat.format(loan.getDueDate().getTime())));
            }
        }
    }
    
    public List<Loan> getList() {
        return loanRepository.all();
    }
}
