package library.services;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import library.LibraryException;
import library.models.Borrower;
import library.repositories.BorrowerRepository;

@Named
@RequestScoped
public class BorrowerService {
    
    @Inject
    private BorrowerRepository borrowers;
    
    public Borrower getBorrower(int id) throws LibraryException {
        
        Borrower borrower = borrowers.get(id);
        
        if (borrower != null)
            return borrower;
        
        throw new LibraryException(String.format("Det finns ingen låntagare med ID #%d", id));
    }
    
    public List<Borrower> getList() {
        
        return borrowers.all();
    }

    public void save(Borrower borrower) {
        
        borrowers.save(borrower);
    }
}
