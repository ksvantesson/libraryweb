package library.services;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import library.LibraryException;
import library.models.Book;
import library.models.BookRecommendation;
import library.repositories.BookRepository;

@Named
@RequestScoped
public class BookService {

    @Inject
    private BookRepository books;
    
    public Book getBook(int id) throws LibraryException {
        
        Book book = books.get(id);
        
        if (book == null)
            throw new LibraryException(String.format("Det finns ingen bok med ID #%d", id));
        
        return book;
    }
    
    public List<Book> getList() {
        
        return books.all();
    }

    public List<BookRecommendation> getRecommendations() {
        
        List<BookRecommendation> recommendations = new ArrayList<>();
        int number = 0;
        
        for (Book book : getList().subList(0, Math.min(3, getList().size()))) {
            
            recommendations.add(new BookRecommendation(book, number++));
        }
        
        return recommendations;
    }
    
    @Transactional
    public void save(Book book) {
        
        books.save(book);
    }

    @Transactional
    public void delete(Book book) {
        
        books.delete(book);
    }
}
