package library.beans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import library.LibraryException;
import library.models.Book;
import library.services.BookService;

@Named
@ViewScoped
//@RequestScoped
public class BookBean implements Serializable {

    @Inject private BookService books;
    
    private Book book;
    
    @PostConstruct
    public void init() {
        
       String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        
        setBook(createBook(id));
    }

    private Book createBook(String id) {
        try
        {
            if (id != null)
                return books.getBook(Integer.parseInt(id));
        }
        catch (LibraryException e) {
            
        }
        
        return new Book();
    }
    
    public String save() {
        books.save(getBook());
        
        return "viewBooks";
    }
    
    public String delete() {
        
        books.delete(getBook());
        
        return "viewBooks";
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
