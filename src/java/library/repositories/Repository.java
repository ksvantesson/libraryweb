package library.repositories;

/*abstract public class Repository<E extends Identity> {
    
    @Inject
    private ObjectMapper mapper;
    private ArrayList<E> entities;
    private final String filename;
    private final Class<E> typeParameterClass;
    
    protected Repository(String filename, Class<E> typeParameterClass) {
        
        this.filename = filename;
        this.typeParameterClass = typeParameterClass;
    }

    public List<E> all() {
        return getEntities();
    }

    public E get(int id) {
        
        for (E entity : getEntities()) {
            
            if (entity.getId() == id)
                return entity;
        }

        return null;
    }
    
    public void delete(E entity) {
        
        getEntities().remove(get(entity.getId()));
        
        saveRepository();
    }

    public void save(E entity) {
        
        if (entity.getId() == 0)
            entity.setId(getNextId());
        else
            delete(entity);
        
        getEntities().add(entity);
        
        saveRepository();
    }

    private void saveRepository() {
        try
        {
            mapper.writeValue(new File(filename), entities);
        }
        catch (IOException e) {
        }
    }

    private ArrayList<E> getEntities() {
        loadRepository();
        
        return entities;
    }

    private int getNextId() {
        return entities.stream().map(e -> e.getId()).max(Integer::compare).orElse(0) + 1;
    }

    private void loadRepository() {
        try
        {
            if (entities == null)
                entities = mapper.readValue(new File(filename), TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, typeParameterClass));
        }
        catch (IOException e) {
        }
        
        if (entities == null)
            entities = new ArrayList<>();
    }

    public List<E> search(Predicate<E> predicate) {
        return getEntities().stream().filter(predicate).collect(Collectors.toList());
    }
}*/
