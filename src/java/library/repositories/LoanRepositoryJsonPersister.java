package library.repositories;

import library.models.Loan;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class LoanRepositoryJsonPersister {
    
    @Inject private ObjectMapper mapper;
    @Inject private BookRepository books;
    @Inject private BorrowerRepository borrowers;
    
    public ArrayList<Loan> load() {
        
        ArrayList<Loan> loans = new ArrayList<>();
        
        try
        {
            for (LoanPersistModel loan : mapper.readValue(new File("loans.json"), LoanPersistModel[].class)) {

                loans.add(toLoan(loan, books, borrowers));
            }
        }
        catch (IOException e) {
            
        }
        
        return loans;
    }

    private static Loan toLoan(LoanPersistModel loan, BookRepository books, BorrowerRepository borrowers) {
        return new Loan(); //books.get(loan.getBookId()), borrowers.get(loan.getBorrowerId()), loan.getDueDate());
    }
    
    public void save(ArrayList<Loan> loans) {

        try
        {
            mapper.writeValue(new File("loans.json"), loans.stream().map(loan -> toModel(loan)).toArray());
        }
        catch (IOException e) {
            
        }
    }

    private LoanPersistModel toModel(Loan loan) {
        
        LoanPersistModel model = new LoanPersistModel();
        
        model.setBookId(loan.getBook().getId());
        model.setBorrowerId(loan.getBorrower().getId());
        model.setDueDate(loan.getDueDate());
        
        return model;
    }
}
    
class LoanPersistModel {
    
    private int bookId;
    private int borrowerId;
    private Calendar dueDate;

    public LoanPersistModel() {
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getBorrowerId() {
        return borrowerId;
    }

    public void setBorrowerId(int borrowerId) {
        this.borrowerId = borrowerId;
    }
    
    public Calendar getDueDate() {
        return dueDate;
    }
    
    public void setDueDate(Calendar dueDate) {
        this.dueDate = dueDate;
    }
}
