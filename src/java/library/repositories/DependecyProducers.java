package library.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Dependent
public class DependecyProducers {
    
    private static final EntityManagerFactory emFactory;
    
    static {
        emFactory = Persistence.createEntityManagerFactory("com.library");
    }
    
    @Produces
    @Dependent
    public ObjectMapper produceObjectMapper() {
        
        return new ObjectMapper();
    }
    
    @Produces
    @Dependent
    public EntityManager produceEntityManager() {
        return emFactory.createEntityManager();
    }
}
