package library.repositories;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import library.models.Book;

@ApplicationScoped
public class BookRepository  {
    @PersistenceContext
    private EntityManager em;
    
    public BookRepository() {
    }
    
    public Book get(int id) {
        return em.find(Book.class, id);
    }
    
    public List<Book> all() {
        return em.createQuery("Select b FROM Book b").getResultList();
    }
    
    public void delete(Book book) {
        try{
            em.remove(get(book.getId()));
        }
        catch (Exception e) {
            
        }
    }
    
    public void save(Book book) {
        try{
            if (book.getId() == 0)
                em.persist(book);
            else
                em.merge(book);
        }
        catch (Exception e) {
            
        }
    }
}
