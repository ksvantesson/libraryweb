package library.repositories;

import library.models.Book;
import library.models.Borrower;
import library.models.Loan;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class LoanRepository {
    
    private ArrayList<Loan> entities;
    
    @Inject
    private LoanRepositoryJsonPersister persister;
    
    public List<Loan> all() {
        return getEntities();
    }

    public void add(Loan loan) {
        getEntities().add(loan);
        
        saveRepository();
    }

    public Loan find(Book book) {
        
        return getEntities().stream().filter(e -> e.getBook().getId() == book.getId()).findFirst().orElse(null);
    }
    
    public List<Loan> find(Borrower borrower) {
        
        return getEntities().stream().filter(e -> e.getBorrower().getId() == borrower.getId()).collect(Collectors.toList());
    }
    
    private ArrayList<Loan> getEntities() {
        
        if (entities == null)
            entities = persister.load();
        
        return entities;
    }

    public void remove(Loan loan) {
        getEntities().remove(loan);
        
        saveRepository();
    }

    private void saveRepository() {
        persister.save(entities);
    }
}
