package library.repositories;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import library.models.Borrower;

@ApplicationScoped
public class BorrowerRepository  {
    @PersistenceContext
    private EntityManager em;
    
    public Borrower get(int id) {
        return em.find(Borrower.class, id);
    }
    
    public List<Borrower> all() {
        return em.createQuery("Select b FROM Borrower b").getResultList();
    }
    
    public void delete(Borrower borrower) {
        
        em.remove(borrower);
    }
    
    public void save(Borrower borrower) {
        em.persist(borrower);
    }
}
