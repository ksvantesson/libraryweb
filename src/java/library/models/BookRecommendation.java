package library.models;

public class BookRecommendation {
    
    private Book book;
    private int number;
    
    public BookRecommendation(Book book, int number) {
        
        this.book = book;
        this.number = number;
    }
    
    public int getNumber() {
        return number;
    }
    
    public String getActive() {

        return number == 0 ? "active" : "";
    }
    
    public String getImage() {
        
        return "missingRecommendation.png";
    }
    
    public Book getBook() {
        
        return book;
    }
}
